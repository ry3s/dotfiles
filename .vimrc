" Basic setting
set fenc=utf-8
set nobackup
set noswapfile
set autoread

" Appearance
set number
set showmode
set showcmd
set cursorline
set t_Co=256
syntax enable
colorscheme slate
" Highlight trailing whitespaces
:highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
:match ExtraWhitespace /\s\+$/

" Edit
set expandtab
set tabstop=4
set shiftwidth=0
set smartindent

" Search
set ignorecase
set smartcase
set wrapscan
set incsearch
set hlsearch
nmap <Esc><Esc> :nohlsearch<Enter>
