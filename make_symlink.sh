#!/bin/bash

set -euo pipefail

create_symlink() {
    src=$1
    dst=$2
    ln -sfv "$(realpath "$src")" "$dst"
}

mkdir -p ~/.config/git
create_symlink git/config ~/.config/git/config
create_symlink git/ignore ~/.config/git/ignore

mkdir -p ~/.config/emacs
create_symlink emacs/init.el ~/.config/emacs/init.el

create_symlink .vimrc ~/.vimrc

create_symlink .tmux.conf ~/.tmux.conf
