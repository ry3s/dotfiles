(eval-and-compile
  (customize-set-variable
   'package-archives '(("org" . "https://orgmode.org/elpa/")
                       ("melpa" . "https://melpa.org/packages/")
                       ("gnu" . "https://elpa.gnu.org/packages/")))
  (package-initialize)
  (unless (package-installed-p 'leaf)
    (package-refresh-contents)
    (package-install 'leaf))

  (leaf leaf-keywords
    :ensure t
    :init
    (leaf leaf-convert :ensure t)
    :config
    (leaf-keywords-init)))

(leaf cus-edit
  :custom `((custom-file . ,(locate-user-emacs-file "custom.el"))))

(leaf cus-start
  :custom
  (tool-bar-mode . nil)
  (scroll-bar-mode . nil)
  (blink-cursor-mode . nil) ; カーソルの点滅をやめる
  (global-hl-line-mode . t) ; カーソル行のハイライト
  (column-number-mode . t) ; 列数を表示する
  (global-display-line-numbers-mode . t) ; 行数を表示する
  (inhibit-startup-message . t) ; スタートアップメッセージを表示しない
  (ring-bell-function . 'ignore) ; ビープ音と画面フラッシュを消す
  (show-paren-mode . t) ; 対応するカッコ
  (electric-pair-mode . t) ; カッコの自動対応
  (electric-pair-delete-adjacent-pairs . nil) ; BSしたときに対応する閉じカッコを消さない
  (scroll-preserve-screen-position . 'always)
  (scroll-conservatively . 1)
  (indent-tabs-mode . nil)
  (require-final-newline . t)
  (fill-column . 100)
  (mac-option-modifier . 'meta)
  (make-backup-files . nil) ; バックアップファイルを作らない
  (delete-auto-save-files . t) ; 終了時にオートセーブファイルを削除
  (confirm-kill-emacs . 'y-or-n-p)
  (bidi-display-reordering . nil) ; 右から左に読む言語に対応させないことで描画高速化
  (dired-listing-switches . "-alh") ; dired kb表示
  (recentf-max-saved-items . 200)
  :config
  (global-auto-revert-mode)
  (defalias 'yes-or-no-p 'y-or-n-p)
  (add-hook 'before-save-hook 'delete-trailing-whitespace) ; 自動で空白を削除
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  (global-set-key (kbd "C-h") 'delete-backward-char)
  (set-language-environment "Japanese")
  (prefer-coding-system 'utf-8)
  (setq-default tab-width 4)
  (when window-system (setq default-frame-alist '((width . 100) (height . 35))))
  (set-face-attribute 'default nil :family "Monaco" :height 130)
  (set-face-italic 'italic nil)
  (unless window-system
    (xterm-mouse-mode)
    (global-set-key (kbd "<mouse-4>") 'scroll-down-line)
    (global-set-key (kbd "<mouse-5>") 'scroll-up-line)
    (menu-bar-mode -1))
  (add-to-list 'load-path (locate-user-emacs-file "./lisp"))
  (add-to-list 'load-path "~/lsp-bridge"))

(when (and (eq system-type 'darwin) (eq window-system 'ns))
  (setq ns-command-modifier 'meta)
  (setq ns-alternate-modifier 'super))

(leaf exec-path-from-shell
  :ensure t
  :when (memq window-system '(mac ns x))
  :config (exec-path-from-shell-initialize))

(leaf doom-themes
  :ensure t
  :custom
  (doom-themes-enable-italic . nil)
  (doom-city-lights-brighter-comments . t)
  :config
  (load-theme 'doom-city-lights t)
  (leaf doom-modeline
    :ensure t
    :custom
    (doom-modeline-icon . nil)
    :init (doom-modeline-mode 1)))

(leaf undo-tree
  :ensure t
  :global-minor-mode global-undo-tree-mode
  :custom (undo-tree-auto-save-history . nil)
  :config
  (global-set-key (kbd "C-M-/") 'undo-tree-redo))

(leaf yasnippet
  :ensure t
  :config (yas-global-mode 1))

;; (when (file-directory-p "~/lsp-bridge")
;;   (require 'lsp-bridge)
;;   (global-lsp-bridge-mode)
;;   (setopt lsp-bridge-enable-inlay-hint nil)
;;   (unless (display-graphic-p)
;;     (with-eval-after-load 'acm (require 'acm-terminal))))

(leaf rainbow-delimiters
  :ensure t
  :hook (prog-mode-hook . rainbow-delimiters-mode))

(leaf vertico
  :ensure t
  :global-minor-mode t
  :config
  (leaf orderless
    :ensure t
    :custom
    (completion-styles . '(orderless)))
  (leaf marginalia
    :ensure t
    :global-minor-mode t)
  (leaf consult
    :ensure t
    :config
    (global-set-key (kbd "C-c r") #'consult-recent-file)))

(leaf dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

(leaf which-key
  :ensure t
  :hook (after-init-hook))

(leaf magit
  :ensure t
  :bind ("C-c g" . magit-status))

(leaf google-c-style
  :ensure t
  :config
  (add-hook 'c-mode-common-hook 'google-set-c-style)
  (add-hook 'c++-mode-hook 'google-set-c-style))

(leaf cmake-mode :ensure t)

(leaf tuareg
  :ensure t
  :mode ("\\.ml\\'")
  :config
  (leaf ocamlformat
    :ensure t
    :custom '(ocamlformat-enable . 'enable-outside-detected-project)
    :hook (before-save-hook . ocamlformat-before-save))
  (leaf merlin
    :ensure t
    :hook (tuareg-mode-hook . merlin-mode))
  (leaf dune
    :ensure t))

(leaf markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode) "\\.md\\'" "\\.markdown\\'")
  :custom '((markdown-fontify-code-blocks-natively . nil)))

(leaf yaml-mode
  :ensure t
  :mode ("\\.ya?ml\\'"))

(leaf dockerfile-mode
  :ensure t
  :mode ("Dockerfile\\'"))

(leaf json-mode
  :ensure t
  :defvar (js-indent-level)
  :config
  (add-hook 'js-mode-hook
            (lambda ()
              (make-local-variable 'js-indent-level)
              (setq js-indent-level 2))))

(leaf go-mode
  :ensure t
  :hook (go-mode-hook . (lambda ()
                          (setq indent-tabs-mode t)
                          (setq tab-width 4))))

(defun my-swap-buffer ()
  "Swap buffers with next window."
  (interactive)
  (let* ((current (selected-window))
         (other (next-window current 0))
         (current-buf (window-buffer current)))
    (unless (or (eq current other)
                (window-minibuffer-p current))
      (set-window-buffer (selected-window) (window-buffer other))
      (set-window-buffer other current-buf)
      (select-window other))))
(global-set-key (kbd "C-c o") #'my-swap-buffer)
