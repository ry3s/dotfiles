alias cp=' cp -i'
alias diff="diff --color"
alias grep='grep --color=auto'
alias k='kubectl'
alias ll='ls -alh --color=auto'
alias ls='ls -F --color=auto'
alias mv=' mv -i'
alias ocaml='rlwrap ocaml'
case "$OSTYPE" in
    darwin*)
        alias rm='trash'
        alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
        ;;
    linux*)
        alias rm=' rm -I'
        alias emacs='emacs -nw'
        ;;
esac

export EDITOR=vim
export LANG=en_US.UTF-8

# Remove duplicates
typeset -U path
# emacs key bind
bindkey -e
# 履歴ファイルの保存先
HISTFILE="$HOME"/.zsh_history
# メモリに保存される履歴の件数
HISTSIZE=10000
# 履歴ファイルに保存される履歴の件数
SAVEHIST=100000
# 重複を記録しない
setopt hist_ignore_dups
# ヒストリに追加されるコマンド行が古いものと同じなら古いものを削除
setopt hist_ignore_all_dups
# historyコマンドは履歴に登録しない
setopt hist_no_store
# スペースから始まるコマンド行はヒストリに残さない
setopt hist_ignore_space
# ヒストリに保存するときに余分なスペースを削除する
setopt hist_reduce_blanks
# ヒストリを共有
setopt share_history

# zsh completion & prompt
source "$HOME/dotfiles/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"
fpath+=(
    "$HOME/dotfiles/zsh/zsh-completions/"
    "$HOME/.asdf/completions"
    "/opt/homebrew/share/zsh/site-functions"(N-/)
    "$HOME/dotfiles/zsh/pure")
autoload -U compinit && compinit
command -v kubectl > /dev/null && source <(kubectl completion zsh)
autoload -U promptinit && promptinit
prompt pure

# asdf
if [ -d "$HOME/.asdf" ]; then
    . "$HOME/.asdf/asdf.sh"
fi

path=(
    "$HOME/dotfiles/bin/"
    "$HOME/.local/bin"(N-/)
    "$HOME/go/bin"(N-/)
    $path)

source "$HOME/dotfiles/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
